class Rectangle {
  constructor(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.width = w;
    this.height = h;
  }
  
  // Returns the center X ordinate
  centerX() {
    return this.x + (this.width/2);
  }
  
  // Returns the center Y ordinate
  centerY() {
    return this.y + (this.height/2)
  }
  
  // This method sets the object x & y to center on the given x & y point
  centerOn(x,y) {
    this.x = x - (this.width/2);
    this.y = y - (this.height/2);
  }
  
  setShowInfo(isDisplayed) {
    this.isDisplayed = isDisplayed;
  }
  
  // Returns true only if the provided coordinates are within the bounds of this object
  inside(px, py) {
    return (px >= this.x && px <= this.x+this.width) && (py >= this.y && py <= this.y+this.height);
  }
  
  // Draws this object onto the canvas in the provided color
  draw(color) {
    fill(color);
    rect(this.x, this.y, this.width, this.height);
    
    if (this.isDisplayed) {
      fill ('black');
      textSize(14);
      textAlign(CENTER,CENTER);
      text('X: ' + this.x, this.centerX(), this.centerY() + 35);
      text('Y: ' + this.y, this.centerX(), this.centerY() + 50);
      text('A: ' + curAngle, this.centerX(), this.centerY() + 65);
    }
  }
  
  label(textStr, size, color) {
    fill(color);
    textSize(size);
    textAlign(CENTER,CENTER)
    text(textStr, this.centerX(), this.centerY()); 

  }
}

let sWidth = 800;
let sHeight = 500;

let sqH = 50;
let sqW = 50;

let bWidth = 100;
let bHeight = 25;

let redSq = new Rectangle(sWidth/2 - (sqW/2), 0, sqW, sqH);
let blueSq = new Rectangle(0, (sHeight/2) - (sqH/2), sqW, sqH);
let lrBtn = new Rectangle((sWidth/3)-bWidth,0, bWidth, bHeight);
let penBtn = new Rectangle( (sWidth/3)*2, 0, bWidth, bHeight);

let right = true;
let speed = 5;
let curSpeed = speed;
let mode = 'leftright';

let startAngle = 0;
let endAngle = 0;

let angleInc = 0.015;
let length = 0;
let curAngle = 0;

function setup() {
  createCanvas(sWidth, sHeight);
  startAngle = PI/4;
  endAngle = -PI/4;
  console.log(startAngle, endAngle);
}

function draw() {
  background('white');

  // Line
  line(blueSq.centerX(), blueSq.centerY(), redSq.centerX(), redSq.centerY());
  
  // Red Rect
  redSq.draw('red');
  
  // Blue Rect
  blueSq.draw('blue');
  
  // buttons
  lrBtn.draw('blue');
  lrBtn.label('Left-Right', 20, 'white');
  
  penBtn.draw('blue');
  penBtn.label('Pendulum', 20, 'white');
  
  if (mode === 'leftright') {
    // Move bSq
    if (right)
    {
      blueSq.x += curSpeed;  
      if (blueSq.x > sWidth-sqW) right = !right;
    } else {
      blueSq.x -= curSpeed;
      if (blueSq.x <= 0) right = !right;    
    }
  } else if (mode === 'pendulum') {
    
    // Swing bSq
    if (right) {
      curAngle += angleInc;
      
    } else {
      curAngle -= angleInc;
    }
    
    if (curAngle <= endAngle) {
      curAngle = endAngle;
      right = !right;
    } else if (curAngle > startAngle) {
      curAngle = startAngle;
      right = !right;
    }

    // Calculate new pos
    let x = redSq.x + length * sin(curAngle);
    let y = redSq.y + length * cos(curAngle);
    blueSq.centerOn(x,y);
  }
}

function setMode(newMode) {
  if (newMode === 'leftright') {
    mode = newMode;
      blueSq.setShowInfo(false);
  } else if (newMode === 'pendulum') {
    let difX = redSq.centerX() - blueSq.centerX();
    let difY = redSq.centerY() - blueSq.centerY();
    length = Math.sqrt((difX * difX) + (difY * difY));
    curAngle = Math.atan2(difY, difX);
    mode = newMode;
    blueSq.setShowInfo(true);
  }
}

function radiansToDegrees(r) {
  return r * (180/Math.PI);
}

function mousePressed() {
  // if in blue square, pause
  if (blueSq.inside(mouseX, mouseY)) { // Blue square
    if (curSpeed > 0) {
      curSpeed = 0;
    } else {
      curSpeed = speed;
    }
  } else if (lrBtn.inside(mouseX, mouseY)) {
    setMode('leftright');
  } else if (penBtn.inside(mouseX, mouseY)) {
    setMode('pendulum');
  } else {
    // else move bluesquare to click Y
    blueSq.y = mouseY;
    setMode(mode);
  }
}

function keyPressed() {
  if (keyCode === DOWN_ARROW) {
    blueSq.y+=2;
    if (blueSq.inside(mouseX, mouseY)) blueSq.y = sHeight - sqH;
  } else if (keyCode === UP_ARROW) {
    blueSq.y -= 2;
    if (blueSq.y < 0) blueSq.y = 0;
  } else if (keyCode === 32) {
    right = !right;
  } else if (keyCode === 81) {  // Q key pauses
    if (curSpeed > 0) {
      curSpeed = 0;
    } else {
      curSpeed = speed;
    }
  }
}