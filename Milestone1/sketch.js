function setup() {
  createCanvas(800, 500);
}

function draw() {
  background(255);

  // Line
  line(25,(height/2), (width/2),25)
  
  // Top Rect
  fill('red');
  rect((width/2)-25, 0,50,50);
  
  
  // Side Rect
  fill('blue');
  rect(0,(height/2)-25,50,50);

}