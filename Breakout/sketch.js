
const buttonHeight = 50;
const buttonWidth = 150;

let state = 'title';
let sWidth = 800;
let sHeight = 700;

let highscore = 0;

let title = null;

let bigger = true;

let game = null;
let paused = true;
let pausedText = '';

function setup() {
    createCanvas(sWidth, sHeight);
    title = new Title('Welcome to', 0, -50, 'p5 Breakout', 0,sHeight + 100, 'Start New Game', -100, -100);

    // TODO Remove before release
//    newGame();
  //  nextLevel();
}

function preload() {
    // load sound clips
}

function draw() {
    background('black');

    if (state === 'title') {
        // title screen
        processTitleTick();
        drawTitle();
    } else if(state === 'play') {
        processGameTick();
        drawGame();
    } else if (state === 'levelanim') {
        // New Level animation
        processLevelAnimTick();
        drawLevelAnim();
    } else if (state === 'gameover') {

    }
}

function processGameTick() {
    let level = game.levels[game.currLevel];
    level.batX = sWidth/2 - (level.batWidth/2);


    if (!paused) {
        // Move ball
        level.ball.x += level.ball.deltaX;
        level.ball.y += level.ball.deltaY;

        if (level.ball.y > hudHeight+level.fieldHeight) {
            // ball fell out of play.
            game.ballsLeft--;
            if (game.ballsLeft <= 0) {
                // game over.
                gameOver();
            } else {
                level.ball.reset();
                paused = true;
                level.started = false;
            }
        }

        level.bat.x = mouseX;
        startX = (sWidth/2) - (level.fieldWidth/2);

        let mouseXmin = startX+borderSize-level.bat.width;
        let mouseXmax = startX+level.fieldWidth-borderSize;

        if (mouseX < mouseXmin) level.bat.x = mouseXmin;
        else if (mouseX > mouseXmax) level.bat.x = mouseXmax;


    } else {
        if (game.levels[game.currLevel].started) {
            pausedText = 'PAUSED\nClick to continue.';
        } else {
            pausedText = 'Click to launch';
        }
    }
}

function drawGame() {
    // Draw HUD
    fill('white');
    rect(2,2,sWidth-3,hudHeight);
    fill('black');
    rect(5,5, sWidth-9, hudHeight-6);

    fill('white');
    textSize(26);
    textAlign(LEFT);

    let levelText = "Level: " + (game.currLevel+1).toString();
    let scoreText = "Score: " + (game.score).toString();
    let ballText = "Balls Left: " + (game.ballsLeft).toString();

    text(levelText, 10, 30);
    text(scoreText, 250, 30);
    text(ballText, 600, 30);

    // Draw Playfield Frame;
    let frameStartY = hudHeight+3
    let level = game.levels[game.currLevel]
    let startX = (sWidth/2) - (level.fieldWidth/2);
    fill('gray');
    rect(startX,frameStartY, level.fieldWidth, borderSize); // Top
    rect(startX, frameStartY, borderSize, level.fieldHeight); // left
    rect((startX+level.fieldWidth)-borderSize, frameStartY, borderSize, level.fieldHeight); // right

    fill('black');
    level.bat.y = (frameStartY+level.fieldHeight)-(level.bat.height*2)
    rect(startX, level.bat.y,level.fieldWidth, level.bat.height);

    // Draw Bricks
    let xOffset = startX+borderSize;
    let yOffset = frameStartY + borderSize + 60;
    let wall = level.wall;
    // Draw Bricks
    for (let rows=0; rows<wall.length; rows++) {
        for (let cols=0; cols<wall[rows].length; cols++) {
            let brick = wall[rows][cols];
            if (brick.isVisible()) {
                fill(brick.getColor());
                rect(brick.x + xOffset, brick.y+yOffset, brick.width, brick.height);
            }
        }
    }

    // Draw Bat
    fill('purple');
    rect(level.bat.x, level.bat.y, level.bat.width, level.bat.height, level.bat.height/2);

    // Draw Ball
    fill('white');
    circle(level.ball.x, level.ball.y, level.ball.d*2);

    // Display Paused message, if appropriate
    if (paused) {
        fill('white');
        textSize(26);
        textStyle(ITALIC)
        textAlign(CENTER, CENTER);
        text(pausedText, sWidth / 2, sHeight / 2);
        textStyle(NORMAL);

    }
}

function processTitleTick() {
    const scrollSpeed = 7;

    if (!title.welcomeInPlace) {
        title.welcomeX = sWidth/2;
        title.welcomeY += scrollSpeed;

        if (title.welcomeY >= sHeight/3) {
            title.welcomeY = sHeight/3;
            title.welcomeInPlace = true;
        }
    } else if (!title.nameInPlace) {
        title.nameX = sWidth/2;
        title.nameY -= scrollSpeed;

        if (title.nameY <= (sHeight/3)+75) {
            title.nameY = (sHeight/3)+75;
            title.nameInPlace = true;
        }
    } else if (!title.buttonInPlace) {
        title.buttonX = (sWidth/2) - (buttonWidth/2);
        title.buttonY = (sHeight/3) * 2;
        title.buttonInPlace = true;
    }
}

function processTitleMouseClick() {
    if ((mouseX >= title.buttonX && mouseX <= title.buttonX + buttonWidth) &&
        (mouseY >= title.buttonY && mouseY <= title.buttonY+buttonHeight)) {
        // Start Level 1
        state = 'levelanim';
        newGame();
        nextLevel();
    }
}

function drawTitle() {
    background('black');

    // Draw Welcome
    fill(title.welcomeColor);
    textSize(title.welcomeSize);
    textAlign(CENTER,CENTER);
    text(title.welcome, title.welcomeX, title.welcomeY);

    // Draw Title
    fill(title.nameColor);
    textSize(title.nameSize);
    textAlign(CENTER,CENTER);
    text(title.name, title.nameX, title.nameY);

    // Draw button
    if (title.buttonInPlace) {
        fill(title.buttonColor)
        rect(title.buttonX, title.buttonY, buttonWidth, buttonHeight, 20);
        fill(title.buttonTextColor);
        textSize(title.buttonSize);
        textAlign(CENTER,CENTER);
        text(title.buttonText, title.buttonX + (buttonWidth/2), title.buttonY + (buttonHeight/2));
    }
}

function processLevelAnimTick() {
  const targetLevelSize = 64;

  if (bigger) {
      game.levelTextSize++;

      if (game.levelTextSize >= targetLevelSize) bigger = !bigger;

  } else {
      game.levelTextSize--;
      if (game.levelTextSize <= 1) {
          game.levelTextSize = 1;
          bigger = !bigger;
          state = 'play';
      }
  }
}

function drawLevelAnim() {
    background('black');
    fill('red');
    textSize(game.levelTextSize);
    textAlign(CENTER,CENTER);
    text(game.levels[game.currLevel].name, sWidth/2, sHeight/2);

}

function nextLevel() {
    game.currLevel++;
    if (game.currLevel >= game.levels.length) game.currLevel = 0;

    let level = game.levels[game.currLevel];

    let batWidth = 75;
    level.bat = new Bat(sWidth/2 - (batWidth/2),-1,20,batWidth);

    state = 'levelanim';
    paused = true;

}

function gameOver() {
    state = 'gameover';
}

function mousePressed() {
    if (state === 'title') {
        processTitleMouseClick();
    } else if (state === 'play') {
        if (paused) {
            paused = false;
            game.levels[game.currLevel].started = true;
        } else {
            nextLevel();
        }
    }
}
