
class Level {
    constructor(name, fieldWidth, fieldHeight, wall, speed) {
        this.name = name;
        this.wall = wall;
        this.speed = speed;

        this.fieldWidth = fieldWidth;
        this.fieldHeight = fieldHeight;

        this.levelTextSize = 0;

        this.bat = null;
        this.started = false;

        this.ball = new Ball(fieldWidth/2+20,fieldHeight/2+100,5,2,2);
    }
}

class Brick {

    constructor (x,y, health, width, height) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.health = health;
        this.getColor();
    }

    getColor() {
        switch (this.health) {
            case 5: this.color = 'blue'; break;
            case 4: this.color = 'green'; break;
            case 3: this.color = 'yellow'; break;
            case 2: this.color = 'orange'; break;
            case 1: this.color = 'red'; break;
            case 0: this.color = 'none'; break;
        }
        return this.color;
    }

    isVisible() {
        return this.health > 0;
    }
}

class Game {
    constructor(levels) {
        this.levels = levels;
        this.currLevel = -1;
        this.ballsLeft = 5;
        this.score = 0;
        this.levelTextSize = 1;
    }
}

function newGame() {
    game = new Game(allLevels);
}

function createLevel1() {
    brickCols = 9;
    brickRows = 5;
    let level1Wall = new Array(brickRows);
    let brickWidth = 50;
    let brickHeight = 20;

    let health = 6;
    // 5 rows, by 9 cols
    for (var rows = 0; rows < 5; rows++) {
        level1Wall[rows] = new Array(brickCols);
        health--;
        for (var cols = 0; cols < 9; cols++) {
            level1Wall[rows][cols] = new Brick((cols*brickWidth), (rows*brickHeight), health, brickWidth, brickHeight);
        }
    }

    return new Level('Level 1', 500, 500, level1Wall, 5);
}

let borderSize = 25;
const hudHeight = 50;
let allLevels = [createLevel1(), createLevel1(), createLevel1(), createLevel1(), createLevel1()];

for (let i=0; i<allLevels.length; i++) {
    allLevels[i].name = "Level " + (i+1).toString();
}
