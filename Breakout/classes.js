

class Ball {
    constructor(x,y,d, deltaX, deltaY) {
        this.x = x;
        this.y = y;
        this.d = d;
        this.deltaX = deltaX;
        this.deltaY = deltaY;
        this.color = 'white';

        this.startingX = x;
        this.startingY = y;
        this.startingDeltaX = deltaX;
        this.startingDeltaY = deltaY;
    }

    reset() {
        this.x = this.startingX;
        this.y = this.startingY;
        this.deltaX = this.startingDeltaX;
        this.deltaY = this.startingDeltaY;
    }
}

class Bat {
    constructor(x,y,height, width) {
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
    }
}

class Title {
    constructor(welcome, welcomeX, welcomeY, name, nameX, nameY, buttonText, buttonX, buttonY) {
        this.welcome = welcome;
        this.welcomeX = welcomeX;
        this.welcomeY = welcomeY;
        this.name = name;
        this.nameX = nameX;
        this.nameY = nameY;
        this.buttonText = buttonText;
        this.buttonX = buttonX;
        this.buttonY = buttonY;

        this.welcomeSize = 26;
        this.welcomeColor = 'white';

        this.nameSize = 64;
        this.nameColor = 'red';
        this.welcomeInPlace = false;
        this.nameInPlace = false;
        this.buttonInPlace = false;

        this.buttonColor = 'white';
        this.buttonTextColor = 'black';
        this.buttonSize = 18;
    }

}
