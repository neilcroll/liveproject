let sWidth = 800;
let sHeight = 500;

let sqH = 50;
let sqW = 50;

let rSqX = sWidth/2;
let rSqY = 0;

let bSqX = 0;
let bSqY = sHeight/2;
let right = true;
let speed = 2;

function setup() {
  createCanvas(sWidth, sHeight);
}

function draw() {
  background(255);
  
  // Line
  line(bSqX+(sqW/2), bSqY, rSqX, rSqY+(sqH/2));
  
  // Red Rect
  fill('red');
  rect(rSqX-(sqW/2), 0,sqW,sqH);  
  
  // Blue Rect
  fill('blue');
  rect(bSqX,(height/2)-25,50,50);
  
  // Move bSq
  if (right)
  {
    bSqX+= speed;  
    if (bSqX > sWidth-sqW) right = !right;
  } else {
    bSqX-= speed;
    if (bSqX <= 0) right = !right;    
  }
  
}