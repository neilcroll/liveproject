let sWidth = 800;
let sHeight = 500;

let sqH = 50;
let sqW = 50;

let rSqX = sWidth/2;
let rSqY = 0;

let bSqX = 0;
let bSqY = (sHeight/2) - (sqH/2);
let right = true;
let speed = 5;
let curSpeed = speed;

function setup() {
  createCanvas(sWidth, sHeight);
}

function draw() {
  background(255);
  
    // Line
  line(bSqX+(sqW/2), bSqY+(sqH/2), rSqX, rSqY+(sqH/2));
  
  // Red Rect
  fill('red');
  rect(rSqX-(sqW/2), 0,sqW,sqH);  
  
  // Blue Rect
  fill('blue');
  rect(bSqX,bSqY,sqW,sqH);
  
  // Move bSq
  if (right)
  {
    bSqX+= curSpeed;  
    if (bSqX > sWidth-sqW) right = !right;
  } else {
    bSqX-= curSpeed;
    if (bSqX <= 0) right = !right;    
  }
}

function mousePressed() {
  // if in blue square, pause
  if ((mouseX >= bSqX && mouseX <= bSqX+sqW) && (mouseY >= bSqY && mouseY <= bSqY+sqH)) {
    if (curSpeed > 0) {
      curSpeed = 0;
    } else {
      curSpeed = speed;
    }
  } else {
    // else move bluesquare to click Y
    bSqY = mouseY;
    
  }
}
 
function keyPressed() {
  if (keyCode === DOWN_ARROW) {
    bSqY+=2;
    if (bSqY > sHeight-sqH) bSqY = sHeight - sqH;
  } else if (keyCode === UP_ARROW) {
    bSqY -= 2;
    if (bSqY < 0) bSqY = 0;
  } else if (keyCode === 32) {
    right = !right;
  } else if (keyCode === 81) {
    if (curSpeed > 0) {
      curSpeed = 0;
    } else {
      curSpeed = speed;
    }
  }
  
  
}